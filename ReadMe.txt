Steps for running the program.

1. Compile the program as "javac RunSHA.java". Use latest JDK that is jdk1.7.
2. Run the program as "JAVA RunSHA "Path to file"". Eg: JAVA RunSHA "F:/z.txt"

The program will print the 256 bit hash on the console.