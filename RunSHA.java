/*
*	SHA 3 Implementaion with following parameters.
*	Message Digest Size = 256 bits
*	Bitrate r = 1088
*	Word Size w = 64
*	Number of Rounds n = 24
*	Capacity c = 512
*	Padding = 10*1
*	b = r+c = 1600
*
*	The above parameters can be tuned as per requirements without changing 
*	the code.
*
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Object;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

public class RunSHA {
	
	private static BigInteger BIT_64 = new BigInteger("18446744073709551615");
	
	/*
	 *	Round Constants as specified by the SHA 3 paper. 
	 */
	private BigInteger[] RC = new BigInteger[] {
			new BigInteger("0000000000000001", 16),new BigInteger("0000000000008082", 16),new BigInteger("800000000000808A", 16),new BigInteger("8000000080008000", 16),
			new BigInteger("000000000000808B", 16),new BigInteger("0000000080000001", 16),new BigInteger("8000000080008081", 16),new BigInteger("8000000000008009", 16),
			new BigInteger("000000000000008A", 16),	new BigInteger("0000000000000088", 16),new BigInteger("0000000080008009", 16),new BigInteger("000000008000000A", 16),
			new BigInteger("000000008000808B", 16),	new BigInteger("800000000000008B", 16),new BigInteger("8000000000008089", 16),new BigInteger("8000000000008003", 16),
			new BigInteger("8000000000008002", 16),	new BigInteger("8000000000000080", 16),new BigInteger("000000000000800A", 16),new BigInteger("800000008000000A", 16),
			new BigInteger("8000000080008081", 16),new BigInteger("8000000000008080", 16),new BigInteger("0000000080000001", 16),new BigInteger("8000000080008008", 16) };
	
	/*
	 *  Rotation Offsets as specified by SHA 3 paper
	 */
	private int[][] r = new int[][] { { 0, 36, 3, 41, 18 },{ 1, 44, 10, 45, 2 }, { 62, 6, 43, 15, 61 },{ 28, 55, 25, 21, 56 }, { 27, 20, 39, 8, 14 } };
	private int w, l, n;

	public RunSHA(int i){
		w = i / 25;									//Width
		l = (int) (Math.log(w) / Math.log(2));		//Length
		n = 12 + 2 * l;								//Number of rounds
	}
	
	
	public static void main(String args[]) throws IOException{
		int bitrate = 1088;
		int capacity = 512;
		int d = 32;
		String message="",str="";
		
		File file = new File(args[0]);
		BufferedReader br = new BufferedReader(new FileReader(file));
		while((str=br.readLine()) != null)
			message = message + str;
		
		byte[] byteArray = message.getBytes();
		message = DatatypeConverter.printHexBinary(byteArray);
		
		RunSHA sha = new RunSHA(1600);
		String hashCode = sha.performHashing(message,bitrate,capacity,d);
		System.out.println("Hash Code : " + hashCode);
		
	}


	
	public String performHashing(String message,int bitrate,int capacity,int d){
		
		//Step 1 : Perform Initialization
		BigInteger s[][] = new BigInteger[5][5];
		for(int i=0;i<5;i++){
			for(int j=0;j<5;j++){
				s[i][j]=new BigInteger("0",16);
			}
		}
		
		//Step 2: Perform Padding
		BigInteger[][] p = performPadding(message,bitrate);
		
		//Step 3 : Keccak Absorb function
		for(int z=0; z<p.length;z++){
			BigInteger tempP[] = p[z];
			for(int i=0;i<5;i++){
				for(int j=0;j<5;j++){
					if ((i + j * 5) < (bitrate / w))
						s[i][j] = s[i][j].xor(tempP[i+5*j]);
				}
			}
		}
		
		keccakF(s);
		
		// Step 4: Keccak Squeeze
		String z = "";
		String str = null;
		
		do{
		for (int i = 0; i < 5; i++)
			for (int j = 0; j < 5; j++)
				if ((5 * i + j) < (bitrate / w)){
			        z = z + getRevertStringInHex(s[j][i]).substring(0, 16);
				}
		}while(z.length()<d*2);	
		
		return z.substring(0,d*2);
	}
	
	
	// Keccak Function
	public BigInteger[][] keccakF(BigInteger A[][]){
		for(int i=0;i<n;i++){
			A = roundB(A,RC[i]);
		}
		return A;
	}
	
	// Keccak Round
	public BigInteger[][] roundB(BigInteger[][] A, BigInteger RC){
		BigInteger[] C = new BigInteger[5];
		BigInteger[] D = new BigInteger[5];
		BigInteger[][] B = new BigInteger[5][5];
		
		
		//Step Theta
		for(int i=0;i<5;i++)
			C[i] = A[i][0].xor(A[i][1]).xor(A[i][2]).xor(A[i][3]).xor(A[i][4]);
		
		for(int i=0;i<5;i++)
			D[i] = C[(i + 4) % 5].xor(rotate(C[(i + 1) % 5], 1));
		
		for(int i=0;i<5;i++)
			for(int j=0;j<5;j++)
				A[i][j] = A[i][j].xor(D[i]);
		
		
		//Step Rho and Pi
		for(int i=0;i<5;i++)
			for(int j=0;j<5;j++)
				B[j][(2*i+3*j)%5] = rotate(A[i][j],r[i][j]);
		
		//Step Xi
		for(int i=0;i<5;i++)
			for(int j=0;j<5;j++)
				A[i][j] = B[i][j].xor(B[(i+1)%5][j].not().and(B[(i+2)%5][j]));
		
		//Step Iota
		A[0][0] = A[0][0].xor(RC);
		
		return A;
	}
	
	
	
	
	private BigInteger rotate(BigInteger x, int n) {
		n = n % w;

		BigInteger leftShift = getShiftLeft64(x, n);
		BigInteger rightShift = x.shiftRight(w - n);

		return leftShift.or(rightShift);
	}
	
	private BigInteger getShiftLeft64(BigInteger value, int shift) {
		BigInteger retValue = value.shiftLeft(shift);
		BigInteger tmpValue = value.shiftLeft(shift);

		if (retValue.compareTo(BIT_64) > 0) {

			for (int i = 64; i < 64 + shift; i++)
				tmpValue = tmpValue.clearBit(i);

			tmpValue = tmpValue.setBit(64 + shift);
			retValue = tmpValue.and(retValue);
		}

		return retValue;
	}
	
	public BigInteger[][] performPadding(String message,int bitrate){
		int noOfBlocks = 0;
		int nn=0;
		String revertString = null;
		message = message + "01";
		
		while(((message.length()*4)%bitrate) != (bitrate - 8)){
			message = message + "00";
		}
		
		message = message + "80";
		noOfBlocks = (message.length()*4)/bitrate;
		
		BigInteger[][] blockArray = new BigInteger[noOfBlocks][];
		blockArray[0] = new BigInteger[1600 / w];
		initializeArray(blockArray[0]);
		
		int count=0,i=0,j=0;
		
		while(nn<message.length()){
			
			if (j > (bitrate / w - 1)) {
				j = 0;
				i++;
				blockArray[i] = new BigInteger[1600 / w];
				initializeArray(blockArray[i]);
			}
			
			count++;
			if(count * 4 % w == 0){
				String subMessage = message.substring(count-w/4, w/4+count-w/4);
				blockArray[i][j] = new BigInteger(subMessage,16);    //blockArray[i][j] represents one 64 bit word
				revertString = getRevertStringInHex(blockArray[i][j]);
				String revString = revertString;
		        for (int ii = 0; ii < subMessage.length() - revString.length(); ii++)
						revertString = revertString + "0";
		        blockArray[i][j] = new BigInteger(revertString, 16);
				j++;
			}
			nn++;
		}
		return blockArray;
	}
	
	
	public void initializeArray(BigInteger arr[]){
		for (int i = 0; i < arr.length; i++)
			arr[i] = new BigInteger("0", 16);
	
	}
	
	private String getRevertStringInHex(BigInteger l) {
		byte[] array = l.toByteArray();
		
		//Logic to reverse the byte array
		if (array == null) {
			return null;
		}

		int i = 0;
		int j = array.length - 1;
		byte tmp;

		while (j > i) {
			tmp = array[j];
			array[j] = array[i];
			array[i] = tmp;
			j--;
			i++;
		}
		
		//Return the string representation of byte array
		return DatatypeConverter.printHexBinary(array);
	}
	
}
